<?php namespace Quasar\Ddd\ValueObject;

abstract class Integer extends AbstractValueObject implements ValueObject {

    public function __construct(?int $value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    public function isEmpty(): bool
    {
        return empty($this->value);
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function equalsTo(ValueObject $other): bool
    {
        return ($other instanceof Integer) && $this->value === (string) $other;
    }

    private function guard($value): void
    {
        if (!is_int($value) && !$this->isNull($value)) {
            throw new \InvalidArgumentException('Incorrect value');
        }
    }
}