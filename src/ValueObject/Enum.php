<?php namespace Quasar\Ddd\ValueObject;

use InvalidArgumentException;

abstract class Enum extends AbstractValueObject implements ValueObject {

    protected $value;

    protected function __construct($value)
    {
        $this->value = $value;
    }

    public static function __callStatic($name, $args)
    {
        return new static(self::values()[$name]);
    }

    public static function fromValue($value)
    {
        static::guard($value);

        return new static($value);
    }

    public static function values(): array
    {
        $reflected = new \ReflectionClass(static::class);

        return $reflected->getConstants();
    }

    protected static function guard($value)
    {
        if (!static::isValid($value)) {
            static::throwExceptionForInvalidValue($value);
        }
    }

    protected static function throwExceptionForInvalidValue($value)
    {
        throw new InvalidArgumentException("Invalid value $value" . self::class);
    }

    protected static function isValid($value)
    {
        return in_array($value, static::values(), true);
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function equalsTo(ValueObject $other): bool
    {
        return $other instanceof Enum && (string) $other === $this->value;
    }
}