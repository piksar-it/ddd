<?php namespace Quasar\Ddd\ValueObject;

abstract class AbstractValueObject {

    protected $value;

    public abstract function __toString(): string;

    public function isNull($value): bool
    {
        return ($this instanceof Nullable) && $value === null;
    }
}