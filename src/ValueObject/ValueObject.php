<?php namespace Quasar\Ddd\ValueObject;

interface ValueObject {

    public function equalsTo(ValueObject $other): bool;

}