<?php namespace Quasar\Ddd\ValueObject;

use Ramsey\Uuid\Uuid as RamseyUuid;

abstract class Uuid extends StringLiteral {

    public function __construct(?string $value = null)
    {
        if ($value === null) {
            $value = static::random();
        }

        $this->guard($value);

        $this->value = $value;
    }

    public static function random(): string
    {
        return RamseyUuid::uuid4()->toString();
    }

    public static function isValidUuid(string $value): bool
    {
        return RamseyUuid::isValid($value);
    }

    private function guard($value): void
    {
        if (!self::isValidUuid($value)) {
            throw new \InvalidArgumentException(
                sprintf('<%s> does not allow the value <%s>.', static::class, is_scalar($value) ? $value : gettype($value))
            );
        }
    }
}