<?php namespace Quasar\Ddd\ValueObject;

abstract class StringLiteral extends AbstractValueObject implements ValueObject {

    public function __construct(?string $value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    public function isEmpty(): bool
    {
        return empty($this->value);
    }

    public function __toString(): string
    {
        return (string) $this->value;
    }

    public function equalsTo(ValueObject $other): bool
    {
        return ($other instanceof StringLiteral) && $this->value === (string) $other;
    }

    private function guard($value): void
    {
        if (!is_string($value) && !$this->isNull($value)) {
            throw new \InvalidArgumentException('Incorrect value');
        }
    }
}