<?php namespace Quasar\Ddd\ValueObject;

abstract class DateTime extends AbstractValueObject implements ValueObject {

    public function __construct($value = null)
    {
        $this->guard($value);

       if ($value instanceof \DateTime) {
            $this->value = $value;
            return;
        }

       if ($this instanceof Nullable && $value === null) {
            $this->value = null;
            return;
        }

        if (is_string($value) && ($this->dateValid($value, 'Y-m-d H:i:s'))) {
            $this->value = \DateTime::createFromFormat('Y-m-d H:i:s', $value);
            return;
        }

        if (is_string($value) && ($this->dateValid($value, 'Y-m-d'))) {
            $this->value = \DateTime::createFromFormat('Y-m-d', $value);
            return;
        }

        $this->value = new \DateTime('NOW');
    }

    public function isEmpty(): bool
    {
        return $this->value === null;
    }

    public function __toString(): string
    {
        return !$this->isEmpty() ? $this->value->format('Y-m-d H:i:s') : '';
    }

    public function equalsTo(ValueObject $other): bool
    {
        return ($other instanceof \DateTime) && $this->value == $other;
    }

    public function getDateTime(): ?\DateTime
    {
        return !$this->isEmpty() ? $this->value : null;
    }

    public function format($format = 'Y-m-d H:i:s'): string
    {
        return $this->value->format($format);
    }

    private function guard($value): void
    {
        if (!($value instanceof \DateTime) && !is_string($value) && !$this->isNull($value)) {
            throw new \InvalidArgumentException('Incorrect value');
        }
    }

    private function dateValid($value, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $value);

        return $d && $d->format($format) === $value;
    }
}