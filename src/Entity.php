<?php namespace Quasar\Ddd;

use Quasar\Ddd\Event\DomainEvent;

abstract class Entity {
    
    public abstract function identity(): IdentityInterface;

    public abstract function toArray(): array;

    public function __get(string $name)
    {
        return $this->toArray()[$name] ?? null;
    }
}