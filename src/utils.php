<?php namespace Quasar\Ddd;

use Quasar\Ddd\Event\DomainEvent;
use Quasar\Ddd\Event\DomainEventPublisher;
use Ramsey\Uuid\Uuid;

function publish(DomainEvent ...$events): void
{
    container()->get(DomainEventPublisher::class)->publish(...$events);
}

function uuid()
{
    return Uuid::uuid4()->toString();
}