<?php namespace Quasar\Ddd\Event;

use Quasar\Ddd\IdentityInterface;

abstract class DomainEvent {

    private $aggregateId;

    private $data = [];

    public function __construct(IdentityInterface $aggregateId, array $data = [])
    {
        $this->aggregateId = $aggregateId;
        $this->data = $data;
    }

    public function aggregateId(): IdentityInterface
    {
        return $this->aggregateId;
    }

    public function data(): array
    {
        return $this->data;
    }

    public function __get(string $key)
    {
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        throw new \InvalidArgumentException('Incorrect argument');
    }

    protected abstract function validateData(array $data = []);
}
