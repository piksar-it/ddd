<?php namespace Quasar\Ddd\Event;

interface DomainEventSubscriber
{
    public static function subscribedTo(): array;

    public function handle(DomainEvent $event): void;
}
