<?php namespace Quasar\Ddd\Event;

class SimpleEventBus implements EventBus {

    private $subscribers;

    public function __construct(iterable $subscribers)
    {
        $this->subscribers = $subscribers;
    }

    public function notify(DomainEvent $event): void
    {
        foreach ($this->subscribers as $subscriberClass) {
            /** @var DomainEventSubscriber $subscriberClass */
            if (in_array(get_class($event), $subscriberClass::subscribedTo())) {
                /** @var DomainEventSubscriber $subscriber */
                $subscriber = container()->get($subscriberClass);

                $this->handle($subscriber, $event);
            }
        }
    }

    private function handle(DomainEventSubscriber $subscriber, DomainEvent $event)
    {
        $subscriber->handle($event);
    }
}