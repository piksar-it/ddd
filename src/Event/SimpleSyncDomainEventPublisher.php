<?php namespace Quasar\Ddd\Event;

final class SimpleSyncDomainEventPublisher implements DomainEventPublisher
{
    private $events = [];
    private $publishedEvents = [];
    private $eventBus;

    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    public function record(DomainEvent ...$domainEvents): void
    {
        $this->events = array_merge($this->events, array_values($domainEvents));
    }

    public function publishRecorded(): void
    {
        foreach ($this->popEvents() as $event) {
            $this->eventBus->notify($event);
        }
    }

    public function publish(DomainEvent ...$domainEvents)
    {
        $this->record(...$domainEvents);

        $this->publishRecorded();
    }

    public function popPublishedEvents()
    {
        $events = $this->publishedEvents;
        $this->publishedEvents = [];

        return $events;
    }

    public function hasEventsToPublish(): bool
    {
        return count($this->publishedEvents) > 0;
    }

    private function popEvents()
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }
}
