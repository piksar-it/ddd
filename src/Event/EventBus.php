<?php namespace Quasar\Ddd\Event;

interface EventBus
{
    public function notify(DomainEvent $event): void;
}