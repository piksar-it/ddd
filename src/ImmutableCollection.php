<?php namespace Quasar\Ddd;

use Closure;

class ImmutableCollection {

    protected $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function toArray()
    {
        return array_map(function ($el) {
            return $el->toArray();
        }, $this->items);
    }

    public function has(Closure $fn) {
        return count(array_filter($this->items, $fn)) > 0;
    }

    public function find(Closure $fn) {
        return array_filter($this->items, $fn);
    }
}