<?php namespace Quasar\Ddd;

trait IdentifiableByUuid {

    protected $uuid;

    public function identity(): IdentityInterface
    {
        return $this->uuid;
    }

}