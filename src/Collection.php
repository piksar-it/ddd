<?php namespace Quasar\Ddd;

use Closure;

class Collection extends ImmutableCollection {

    protected $type = null;

    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            $this->add($item);
        }
    }

    public function add($item) {
        $this->guard($item);
        
        $this->items[] = $item;
    }

    protected function guard($item)
    {
        if ($this->type === null) {
            return;
        }

        if ($this->type === 'int' && is_int($item)) {
            return;
        }

        if ($this->type === 'string' && is_string($item)) {
            return;
        }

        if ($this->type === 'float' && is_float($item)) {
            return;
        }

        if (class_exists($this->type) && $item instanceof $this->type) {
            return;
        }

        throw new \TypeError('Type does not match');
    }
}