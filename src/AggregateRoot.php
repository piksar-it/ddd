<?php namespace Quasar\Ddd;

use Quasar\Ddd\Event\DomainEvent;

abstract class AggregateRoot extends Entity {

    private $domainEvents = [];

    protected function __construct() {}

    public abstract function restoreFromMemento(array $snapshot): void;

    public static function newFromMemento(array $snapshot): self
    {
        $aggregate = new static();
        $aggregate->restoreFromMemento($snapshot);

        return $aggregate;
    }

    public abstract function toMemento(): array;

    public function toArray(): array
    {
        return $this->toMemento();
    }

	public function record(DomainEvent $domainEvent): void
	{
        $this->domainEvents[] = $domainEvent;
	}

    final public function pullDomainEvents(): array
    {
        $domainEvents = $this->domainEvents;
        
        $this->domainEvents = [];

        return $domainEvents;
    }
}